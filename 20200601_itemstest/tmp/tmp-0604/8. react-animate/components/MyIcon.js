// 函式元件
import React, { Fragment } from 'react';

// https://github.com/react-icons/react-icons
import { FaAndroid } from 'react-icons/fa';

import { Button } from 'react-bootstrap';


class MyNavBar extends React.Component {
render() {
    return (
        <Fragment>
            <Button variant="primary">
                <FaAndroid />
            </Button>
        </Fragment>
    )
}
}
export default MyNavBar;