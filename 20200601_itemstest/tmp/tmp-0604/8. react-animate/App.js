import React, { useState, useEffect } from 'react';

//https://www.react-reveal.com/docs/
import Zoom from 'react-reveal/Zoom';

import MyIcon from './components/MyIcon';

function App(props) {
    
    return (
        <>
            <Zoom>
                <p>https://www.react-reveal.com/docs/</p>
                <p>Markup that will be revealed on scroll</p>
            </Zoom>
            <MyIcon/>
        </>
    )
}

export default App;