import React from 'react';
import ReactDOM from 'react-dom';

// npm install --save node-sass bootstrap react-bootstrap react-icons
// import './index.css';
import './index.scss';

import App from './App.js';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
