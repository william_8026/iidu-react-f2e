import React from 'react';
import $ from 'jquery';

class App extends React.Component{
    constructor(props) {
        super(props)
        // product = { name: string, price: numbers }
        this.state = {
            data: 0,
        }
    }

    componentDidMount(){
        $('#one').on('click', ()=>{
            alert('data is: ' + this.state.data)
        })
    }

    render() {
        return(
            <>
                <button className="btn btn-outline-success" id="one">click me</button>
                <button className="btn btn-outline-danger" onClick={() => {
                    this.setState({ data: this.state.data+1 })
                }}>
                change to 111
                </button>
                <span style={{ color:'red', fontSize:'3rem' }}>william good!</span>
            </>
        )
    }
}


export default App;