import React, { useState, useEffect } from 'react';
// import { Container, Row, Col, Form, Button, Modal } from 'react-bootstrap';
import MyNavBar from './components/MyNavBar';
import MyFooter from './components/MyFooter';



function App(props) {
    
    return (
        <>
            <MyNavBar/>
            <MyFooter/>

        </>
    )
}

export default App;