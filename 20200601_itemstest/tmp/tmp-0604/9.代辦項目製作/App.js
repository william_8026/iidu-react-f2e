import React, { useState} from 'react';

import MyIcon from './components/MyIcon';

function App(props) {
    
    const [todos, setTodos] = useState(['打代碼？', '出去玩？']);
    const [items, setItems] = useState(['']);

    return (
        <>
            <main className="flex-shrink-0" role="main">
                <div className="container">
                    <h1 className="mt-5">代辦事項</h1>
                    <hr/>
                    <div>
                        <input 
                            type="text" 
                            value={items} 
                            onChange={(event) => {
                                setItems(event.target.value)
                            }}
                            onKeyDown={(event)=>{
                                console.log(event.keyCode);
                                if(event.keyCode === 13 && event.target.value !== "") {
                                    const newTodos = [event.target.value, ...todos]
                                    setTodos(newTodos)
                                    setItems('')
                                }
                            }}
                        /><MyIcon/>
                    </div>
                    <div>
                        <ul>
                            { 
                                todos.map((value, index) => {
                                    return (
                                        <li key={index}>{value}</li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </main>
        </>
    )
}

export default App;