import React from 'react';

class SelectBox extends React.Component {
    constructor(props) {
        super(props)
        this.state = { value: '' }
    }

    handleChange = (event) => {
        // 先保存 值 
        // 先取道值，此方法是已經取到更新後的值，下面console會一起改變
        const values = event.target.value;
        // 異步 setState
        // values 完成就會呼叫 後面的callback function
        this.setState({value: values}, ()=>{
            console.log(`in cakkback setState:${this.state.value}`)
        })

        console.log(`out cakkback setState:${this.state.value}`)
    }

    render() {
        return (
            <div>
            <select onChange={this.handleChange} value={this.state.value}>
                <option value="JavaScript" key={1}>
                JavaScript
                </option>
                <option value="Angular2" key={2}>
                Angular2
                </option>
                <option value="React" key={3}>
                React
                </option>
            </select>
            <h1>{this.state.value}</h1>
            </div>
        )
    }
}

export default SelectBox;