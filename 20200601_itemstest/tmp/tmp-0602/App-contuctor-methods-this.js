import React , {Component}from 'react';


class Appclass extends Component {
  constructor (props){
    super(props); // 是指 父母的類別 (上一層)

    // 宣告狀態
    this.state = {
      total: 0,
    };

    // 使用箭頭函數，就不用寫 用bind() 綁定this 
    // 以下是 手動綁定this
    // 使用箭頭函數 是 自動綁定 this 
    // this.handleClick = this.handleClick.bind(this);
  }
  
  handleClick(value) {
    this.setState({total: this.state.total + value})
  };


  render() {    
    return (
      <div>
        <button onClick={ ()=>{
          this.handleClick(1)
        } }
        >add + </button>
        <button onClick={ ()=>{
          this.handleClick(-1)
        } }
        >edit - </button>
        <div id="total">{this.state.total}</div>
      </div>
    );
  }
}

export default Appclass;
