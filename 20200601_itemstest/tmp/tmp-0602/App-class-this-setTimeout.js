import React , {Component}from 'react';


class AppSetTimeout extends Component {
  constructor (props){
    super(props); // 是指 父母的類別 (上一層)
    // this.handleClickAfterSeconds = this.handleClickAfterSeconds.bind(this);
    // 宣告狀態
    this.state = {
      total: 0,
    }
  }

  // 只會執行一次
  handleClickAfterSeconds = (value) => {
    setTimeout(() => {
      console.log(this);
      this.setState({ total: this.state.total + value })
    },1000)
  }

  // 無限每秒+1
  handleClickEverySeconds = (value) => {
    setInterval(()=> {
      console.log(this);
      this.setState({ total: this.state.total + value })
    },1000)

  }

  render() {    
    return (
      <div>
        {/* <button onClick={ 
          this.handleClickAfterSeconds(1)
        }
        >add + </button>
       */}

        <button onClick={ ()=>{
          this.handleClickAfterSeconds(1)
        } }
        >add + </button>
        <button onClick={ ()=>{
          this.handleClickEverySeconds(-1)
        } }
        >edit - </button>
        
        <div id="total">{this.state.total}</div>

      </div>
    );
  }
}

export default AppSetTimeout;
