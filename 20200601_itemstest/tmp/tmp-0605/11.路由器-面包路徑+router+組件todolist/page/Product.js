import React from 'react'
import { withRouter } from 'react-router-dom'

import MyBanner from '../components/todo/MyBanner'
import MyBreadcrumb from '../components/MyBreadcrumb'

function Product(props) {
    console.log(props)
    return (
        <>
            <MyBanner title="關於產品" lead="'關於產品'是一個網站的重點"/>
            <MyBreadcrumb/>
            <h2>目前的產品編號是(從網址得上得到)：{props.match.params.id}</h2>
        </>
    )
}

// 高階元件的用法
export default withRouter(Product)