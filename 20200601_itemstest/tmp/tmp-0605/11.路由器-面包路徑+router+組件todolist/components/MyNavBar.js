// 函式元件
import React, { Fragment } from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';

// https://github.com/eyesofkids/201/blob/master/data/0605/react-router%E4%BD%BF%E7%94%A8%E8%AA%AA%E6%98%8E.md
import { NavLink } from 'react-router-dom';

function MyNavBar(props) {

    return (
        // https://react-bootstrap.github.io/components/navbar/
        <Fragment>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">

                        {/* react bootstrap */} 
                        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/about">About</Nav.Link>
                        <Nav.Link as={NavLink} to="/product">prodcut</Nav.Link>
                        <Nav.Link as={NavLink} to="/product2">prodcut v5.1 </Nav.Link>
                        <Nav.Link as={NavLink} to="/todoapp">ToDoApp</Nav.Link>

                        {/* 無css 樣式 */}
                        {/* as={NavLink} 就會有 css 樣式 */}
                        {/* <Link as={NavLink} to="/" exact> 首頁 </Link>
                        <Link as={NavLink} to="/about"> 關於我們 </Link>
                        <Link as={NavLink} to="/todoapp"> 代辦事項 </Link>  */}
                        
                        {/* 導覽頁面專用 因為有css 樣式 */}
                        {/* <NavLink to="/" exact> 首頁 </NavLink>
                        <NavLink to="/about"> 關於我們 </NavLink>
                        <NavLink to="/todoapp"> 代辦事項 </NavLink> */}

                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </Fragment>
    )
}
export default MyNavBar;