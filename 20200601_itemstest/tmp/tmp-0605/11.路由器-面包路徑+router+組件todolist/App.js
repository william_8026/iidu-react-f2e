import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import React, { useState } from 'react';

import MyNavBar from './components/MyNavBar';
import MainContent from './components/MainContent';

// page router 組件
import Home from './page/Home'
import About from './page/About'
import TodoApp from './page/TodoApp'
import Product from './page/Product'
import Product2 from './page/Product2'

function App(){
    const [todos, setTodos] = useState([
        { id: 1591256594282, text: '買牛奶', completed: false, edited: false },
        { id: 1591256594281, text: '買iphone', completed: false, edited: false },
        { id: 1591256594283, text: '學react', completed: false, edited: false },
    ])

    return(
        <Router>
            <>
                <MyNavBar/>
                <MainContent title = "william 測試跳轉">

                    {/* 連結 */}
                    {/* <Link to="/">_首頁_</Link>
                    <Link to="/about">_關於我們_</Link>
                    <Link to="/todoapp">_代辦事項_</Link> */}

                    {/* 切換地址 */}
                    <Switch>
                        <Route exact path="/">
                            <Home />
                        </Route>
                        <Route path="/about">
                            <About />
                        </Route>
                        <Route path="/product/:id?">
                            <Product />
                        </Route>
                        <Route path="/product2/:id?">
                            <Product2 />
                        </Route>
                        <Route path="/todoapp">
                            <TodoApp todos={todos} setTodos={setTodos}/>
                        </Route>
                    </Switch>

                </MainContent>
            </>
        </Router>
    )
}

export default App