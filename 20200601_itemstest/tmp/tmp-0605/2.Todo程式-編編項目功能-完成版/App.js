import React, { useState } from 'react';
import TodoAddFrom from './components/TodoAddFrom';
// import TodoItem from './components/TodoItem';
import TodoLIst from './components/TodoLIst';
import MyNavBar from './components/MyNavBar';
import MainContent from './components/MainContent';

// 函式元件
function App() {
    // todos列表
    // todo = { id: 123213, text:'買牛奶', completed: false }
    const [todos, setTodos] = useState([
        { id: 1591270635945, text: '買牛奶', completed: false, edited: false },
        { id: 1591256594281, text: '買iphone', completed: false, edited: false },
        { id: 1591256594283, text: '學react', completed: false, edited: false},
        ])
    
        // 用於文字輸入框輸入新的todo
        const [text, setText] = useState('')
    
        // 利用id值尋找符合的todos裡的index，然後改變completed
        const handleCompleted = (id) => {
            const newTodos = [...todos]
        
            const todoItemIndex = todos.findIndex((v, i) => v.id === id) // 返回 索引值 
        
            if (todoItemIndex !== -1) {
                newTodos[todoItemIndex].completed = !newTodos[todoItemIndex].completed
                setTodos(newTodos)
            }
        }

        // 利用id值尋找符合的todos裡的index，然後改變edited值
        const handleEditedToggle = (id) => {
            const newTodos = [...todos]

            const todoItemIndex = todos.findIndex((v, i) => v.id === id)

            if (todoItemIndex !== -1) {
            newTodos[todoItemIndex].edited = !newTodos[todoItemIndex].edited
            setTodos(newTodos)
            }
        }

        // 利用id值尋找符合的todos裡的index，然後改變text值
        const handleEditedSave = (id, text) => {
            const newTodos = [...todos]

            const todoItemIndex = todos.findIndex((v, i) => v.id === id)

            if (todoItemIndex !== -1) {
            newTodos[todoItemIndex].text = text
            setTodos(newTodos)
            }

            handleEditedToggle(id)
        }

        const handleDelete = (id) =>{
            // filter() 會回傳一個陣列
            const newTodos = todos.filter((values,index)=>{ return values.id !== id })
            setTodos(newTodos)
        }

        return (
        <>
            <MyNavBar/>
            <MainContent title = "待辨事項">
                <TodoAddFrom
                    text={text}
                    todos={todos}
                    setTodos={setTodos}
                    setText={setText}
                    allProps={{
                        text,
                        todos,
                        setText,
                        setTodos,
                    }}
                />

                <TodoLIst 
                    todos={todos} 
                    handleCompleted={handleCompleted}
                    handleDelete={handleDelete}
                    handleEditedToggle={handleEditedToggle}
                    handleEditedSave={handleEditedSave}
                />
            </MainContent>
        </>
    )
}

export default App