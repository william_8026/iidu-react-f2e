import React from 'react';

function TodoItem (props){
    console.log("props:"+ props);
    const {value, handleCompleted} = props;
    // 利用id即為加入的時間日期
    const date = new Date(value.id)
    
    return (
        <>
            <li
                // key={value.id}
                className="list-group-item d-flex justify-content-between align-items-center"
            >
                <span style={{ color: 'red' }}>
                <del>{value.text}</del>
                </span>
                <button
                type="button"
                className="btn btn-secondary"
                onClick={() => {
                    handleCompleted(value.id)
                }}
                >
                取消完成
                </button>
                <span className="badge badge-primary badge-pill">
                {date.toLocaleString()}
                {/* 2020/6/4 下午11:59:01 */}
                </span>
            </li>
        </>
    )
}

export default TodoItem