import React from 'react';
import TodoItem from './TodoItem';
import TodoItemEditForm from './TodoItemEditForm';

function TodoLIst(props){
    // console.log(props)  
    
    const { todos, handleCompleted, handleDelete, handleEditedToggle, handleEditedSave } = props;
    // console.log(todos)
    return(
        <>
            <div>
                <ul className="list-group">
                    {todos.map((value, index) => {
                        // 編輯 edited =true 
                        if (value.edited) {
                            return (
                                <TodoItemEditForm
                                    key={value.id}
                                    value={value}
                                    handleEditSave={handleEditedSave}
                                />
                            )
                        }
                        // 編輯 edited =false 改用 TodoItem
                        return (
                            <TodoItem
                                key={value.id}
                                value={value}
                                handleCompleted={handleCompleted}
                                handleDelete={handleDelete}
                                handleEditedToggle={handleEditedToggle}
                            />
                        )
                    })}
                </ul>
            </div>
        </>
    )
}

export default TodoLIst;