import React,{ useState } from 'react';
import MyTextInput from './components/MyTextInput';
import MyTextDisplay from './components/MyTextDisplay';

function App() {

  const [nameFromChold, setNameFromChold] = useState(''); // hooks
                                                          // nameFromChold 是得到值 get
                                                          // setNameFromChold 是接收的方法 set 

  return (
    <div>
      {/* 將 sendNameToMe 傳入 子組件，並且用 setNameFromChold 取得值 */}
      <MyTextInput 
        sendNameToMe={(nameValue)=>{setNameFromChold(nameValue)}}
      />
      {/* <MyTextInput 
        onChange={(event) => setNameFromChold(event.target.value)}
      /> */}

      <MyTextDisplay text={nameFromChold} />
    </div>
  );
}

export default App;
