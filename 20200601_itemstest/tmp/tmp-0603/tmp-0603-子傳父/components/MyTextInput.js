import React, { useState, Fragment } from 'react';

function MyTextInput(props) {

    const [name, setName] = useState('');

    return (
        <Fragment>
            <input
                type="text"
                value={name}
                onChange={(event) => setName(event.target.value)}
            />

            {/* <input type="text" value={props.value} onChange={props.onChange} /> */}
            {/* 相當於以下的語法 */}
            {/* <input type="text" {...props} /> */}

            <button onClick={() => {
                    props.sendNameToMe(name)
                }}
            >送到 父母 元件</button>
        </Fragment>
    )
}

export default MyTextInput;
