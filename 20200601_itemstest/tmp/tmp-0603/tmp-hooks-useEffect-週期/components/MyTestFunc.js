import React, { useState, useEffect } from 'react'

// 所有狀態與更動狀態均來自於上層父母元件
function MyTestFunc(props) {
  const [total, setTotal] = useState(100)

  // https://github.com/eyesofkids/201/issues/13
  
  // For componentDidMount
  useEffect(() => {
    console.log('MyTestFunc Child componentDidMount')
  }, [])


  // For componentDidUpdate
  useEffect(() => {
    // Your code here
    console.log('MyTestFunc Child componentDidUpdate')
  }, [total])


  // componentWillUnmount
  useEffect(() => {
    return () => {
      // Your code here
      console.log('MyTestFunc Child componentWillUnmount')
    }
  }, [])


  return (
    <>
      <h1>MyTestFunc</h1>
      <div onClick={() => setTotal(total + 1)}>{total}</div>
    </>
  )
}

export default MyTestFunc;