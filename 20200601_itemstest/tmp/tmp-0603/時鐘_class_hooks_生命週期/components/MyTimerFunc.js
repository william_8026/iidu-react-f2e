// 函式元件
import React, { useState, useEffect, Fragment } from 'react';

function MyTimerFunc(props) {
  const [ date, setDate ] = useState(new Date());

  // https://github.com/eyesofkids/201/issues/13
  useEffect(() => {
    console.log('DidMount')
    const timerID = setInterval(() => {
      setDate(new Date())
    }, 1000)

    return () => {
      window.clearInterval(timerID)
    }
  }, [date]);

  return (
  <Fragment>
    <h1>{date.toLocaleTimeString()}</h1>
  </Fragment>
  )
}
export default MyTimerFunc;