// 類別元件
import React ,{Fragment} from 'react';

class MyTimerClass extends React.Component {
  // Mounting(掛載階段) 只會呼叫一次
  constructor(props) {
    // 呼叫React.Component建構式
    super(props)

    //設定state初始值
    this.state = { date: new Date() }
  }

  // render完畢觸發
  componentDidMount() {
    console.log('DidMount')
    this.timerId = setInterval(() => {
      this.setState({ date: new Date() })
    }, 1000)
  }


  // 卸載頁面
  // 當一個 component 被從 DOM 中移除時，這個方法將會被呼叫：
  componentWillUnmount() {
    // 以下暫時無用
    console.log('WillUnmount')
    clearInterval(this.timerId)
  }

  render() {
    return (
      <Fragment>
        <h1>{this.state.date.toLocaleTimeString()}</h1>
      </Fragment>
    )
  }
}

export default MyTimerClass;