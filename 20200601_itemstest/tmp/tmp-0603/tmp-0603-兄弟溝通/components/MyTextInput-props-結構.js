import React, { useState, Fragment } from 'react';

function MyTextInput(props) {

    const { value, onChange } = peops; //解構賦值 
    return (
        <Fragment>
            <input type="text" value={value} onChange={onChange} />
            {/* 相當於以下的語法  */}
            <input type="text" {...props} />
        </Fragment>
    )
}

export default MyTextInput;
