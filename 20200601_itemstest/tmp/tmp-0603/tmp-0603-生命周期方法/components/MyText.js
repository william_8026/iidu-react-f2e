import React from 'react'

// 類別型元件
class MyText extends React.Component {
  // 掛載只會呼叫一次
  constructor(props) {
    // 呼叫React.Component建構式
    super(props)

    //設定state初始值
    this.state = {
      total: 0,
    }
    console.log('child constructor')
  }

  componentDidMount() {
    console.log('child componentDidMount')
  }

  componentDidUpdate() {
    console.log('child componentDidUpdate')
  }

  render() {
    // 頁面進入 render -> componentDidMount 
    // 數據更新 render -> componentDidUpdate 
    console.log('render')
    return (
      <>
        <button
          onClick={() => {
            this.setState({ total: this.state.total + 1 })
          }}
        >
          +1
        </button>
        <button
          onClick={() => {
            this.setState({ total: this.state.total - 1 })
          }}
        >
          -1
        </button>
      </>
    )
  }
}

export default MyText