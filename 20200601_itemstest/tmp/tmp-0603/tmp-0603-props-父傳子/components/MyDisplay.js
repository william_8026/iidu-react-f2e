import React from 'react';
import PropTypes from 'prop-types'; // ES6

function MyDispaly(props){
    console.log(props);
    return (
        <div>
            <div>{props.TextTotals}</div>
        </div>
    )
}

MyDispaly.propTypes = { 
    TextTotals: PropTypes.number.isRequired, // 要求使用者 希望使用 string 而不是其他
                                             // isRequired 檢查
}

// Specifies the default values for props:
MyDispaly.defaultProps = {
    TextTotals: '請給值，謝謝。'
  };

export default MyDispaly;