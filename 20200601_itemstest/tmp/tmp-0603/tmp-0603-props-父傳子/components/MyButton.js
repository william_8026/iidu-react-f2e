import React ,{Fragment}from 'react';

function MyButton(props){
    console.log(props);
    return (
        <Fragment>
            <button onClick={props.onClickMethod}
            >{props.title}</button>
        </Fragment>
    )

}

export default MyButton;