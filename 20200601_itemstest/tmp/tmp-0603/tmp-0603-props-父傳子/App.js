import React,{ useState } from 'react';
import MyDisplay from './components/MyDisplay';
import MyButton from './components/MyButton';

function App() {
  const[total, setTotal] = useState(999); // useState 是api
                                          // total 取值
                                          // setTotal 設置值
  return (
    <div>
      <MyButton 
        title="add -" 
        onClickMethod={()=>{
          setTotal(total - 1);
        } 
      }/>
      <MyButton 
        title="add +" 
        onClickMethod={()=>{
          setTotal(total + 1);
        } 
      }/>
      <MyDisplay TextTotals={total}/>
    </div>
  );
}

export default App;
