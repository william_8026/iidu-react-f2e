import React, { useState, useEffect, createContext, useRef } from 'react'
import { withRouter } from 'react-router-dom'

const redirectAfterLogin = {
    light: {
        foreground: "#000000",
        background: "#eeeeee"
    },
    dark: {
        foreground: "#ffffff",
        background: "#222222"
    }
};

export const RegisterStorage = createContext(redirectAfterLogin)

export const withRegisterProvider = WrappedComponent => {

    const RegisterProvider = (props) => {

        return (
            <RegisterStorage.Provider value={redirectAfterLogin}>
                <WrappedComponent {...props} />
            </RegisterStorage.Provider>
        )
    }

    return withRouter(RegisterProvider)
}

export const withRegisterConsumer = WrappedComponent => props => {
    return(
        <RegisterStorage.Consumer>
            {val => (
                <WrappedComponent {...props} registerData={val} />
            )}
        </RegisterStorage.Consumer>
    )
}