import React, { createContext, useContext, useReducer } from 'react';

// 建立一個 Context
const ContextStore = createContext({
  todos: []
});

// 使用 ContextStore （Provider 提供者）
function UseContextStore() {
  return (
    <ContextStore.Provider value={{todos: ['run']}}>
      <Todos />
    </ContextStore.Provider>
  )
};

// Todos （Consumer 消費者）
function Todos() {
  return (
    <ContextStore.Consumer>
      {value => value.todos.map(todo => <div key={todo}>todo / {todo}</div>)} 
    </ContextStore.Consumer>
  )
};

export default UseContextStore;