import React, { createContext, useContext, useReducer } from 'react';

// useReducer(reducer, initialState)
// reducer 自訂 reducer action 內容
// initialState 自訂 state 初始內容

// 首先為了將 state 綁在一起，需要先建立一個 Global State Context
const productsInitState = { products: [] }
const ContextStore = createContext({
  products: [],
})

function productsReducer(state, action) {
  switch(action.type) {
    case 'ADD_PRODUCT':
      return {
        ...state,
        products: state.products.concat({ id: state.products.length })
      }
    default:
      return state
  }
}

function Products() {
  const { products, productDispatch } = useContext(ContextStore);
  return (
    <div>
      {
        products.map(product => (<div key={product.id} >PRODUCT - {product.id}</div>))
      }
      <button onClick={() => productDispatch({type: 'ADD_PRODUCT'})}>ADD PRODUCT</button>
    </div>
  )
}

function UseContextStore() {
  const [productState, productDispatch] = useReducer(productsReducer, productsInitState);
  
  return (
    <ContextStore.Provider
      value={{
        products: productState.products,
        productDispatch,
      }}
    >
      <Products />
    </ContextStore.Provider>
  )
}


export default UseContextStore;