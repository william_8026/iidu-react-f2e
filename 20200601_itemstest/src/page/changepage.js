import React, { useState, useContext } from 'react'
import { withRouter } from 'react-router-dom'

import classnames from 'classnames';

import MyBanner from '../components/todo/MyBanner'
import MyBreadcrumb from '../components/MyBreadcrumb'
import {RegisterStorage} from './UseContext'


function Changepage() {
    const [num, setNum] = useState({ tabIndex: 0 })
    const registerData = useContext(RegisterStorage);
    console.log('registerData:', registerData)
    return (
        <>
            <MyBanner title="點擊頁面和換內容" lead="點擊頁面和換內容"/>
            <MyBreadcrumb />
            <div>
                <button className={classnames("left", "stats-button", num.tabIndex === 0 ? "selected" : null)} onClick={(e) => { setNum({ tabIndex: 0 }) }}>
                    0
                </button>
                <button className={classnames("left", "stats-button", num.tabIndex === 1 ? "selected" : null)} onClick={(e) => { setNum({ tabIndex: 1 }) }}>
                    1
                </button>
                <button className={classnames("left", "stats-button", num.tabIndex === 2 ? "selected" : null)} onClick={(e) => { setNum({ tabIndex: 2 }) }}>
                    2
                </button>
            </div>
            <div>
                {num.tabIndex === 0 && <div>000000</div>}
                {num.tabIndex === 1 && <div>111111</div> }
                {num.tabIndex === 2 && <div>222222</div> }
            </div>

        </> 
    )
}

// 高階元件的用法
export default withRouter(Changepage)