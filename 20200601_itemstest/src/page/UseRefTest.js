import React, { useRef } from "react";


const UseRefTest = () => {
  const inputRef = useRef(null); // 1

  const handleClick = () => {
    console.log(inputRef)
    console.log(inputRef.current)
    inputRef.current.focus(); // 3
  };

  return (
    <div>
      {/* 2 */}
      <input type="text" ref={inputRef} />
      <button onClick={handleClick}>click me to focus input</button>
      <button onClick={()=>{handleClick()}}>click me to focus input</button>
    </div>
  );
};


export default UseRefTest;