import React, { createContext, useContext } from 'react'
import { ParentContext } from './UseContextStore-simple.js'

// 下層組件
const Son = () => {
  // 將 Context Component 放進 useContext 中取得 value 的資料
  const title = useContext(ParentContext)
  return (
    // render 出 value 的資料
    <h1>{title}</h1>
  )
}

export default Son
