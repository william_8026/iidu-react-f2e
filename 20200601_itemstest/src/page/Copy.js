import React , {useRef} from 'react'
import { withRouter } from 'react-router-dom'

import MyBanner from '../components/todo/MyBanner'
import MyBreadcrumb from '../components/MyBreadcrumb'


function Copy() {

    // 什麼是 useRef ？
    // useRef 是一個可以讓我們抓取到 DOM 節點的 hooks 
    // useRef 回傳的是一個物件
    // 綁定的 DOM node 做操作需要到.current 
    // https://ithelp.ithome.com.tw/articles/10221937?sc=rss.iron
    
    const textAreaRef = useRef(null);

    const copyToClipboard =(e)=> {
        // console.log(e)
        console.log(textAreaRef)
        console.log(textAreaRef.current)
        // console.log(document.execCommand('copy'))
        textAreaRef.current.select();
        document.execCommand('copy');
        e.target.focus();
        setTimeout(()=>{
            alert("copy 成功")
        },1000)
    };

    const cutToClipboard =(e)=> {
        console.log(e)
        // console.log(document.execCommand('copy'))
        textAreaRef.current.select();
        document.execCommand('cut');
        e.target.focus();
        setTimeout(()=>{
            alert("cut 成功")
        },1000)
    };
    
    return (
        <>
            <MyBanner title="Copy 複製&剪下功能" lead="Copy"/>
            <MyBreadcrumb />
            <input ref={textAreaRef} />
            <div onClick={(e)=>copyToClipboard(e)}>Copy 複製</div> 
            <div onClick={(e)=>cutToClipboard(e)}>Cut 剪下</div> 
        </>
    )
}

// 高階元件的用法
export default withRouter(Copy)