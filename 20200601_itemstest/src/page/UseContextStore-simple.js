import React, { createContext, useContext } from 'react';
import Children from './UseContextStore-simple-children'
// 建立一個 Context Component
export const ParentContext = createContext();

// 上層組件
const UseContextStore = (props) => {
    console.log(props)
    // 要與下層組件共享的資料
    const title = 'Hi this is context!'
    return (
        // 將要傳遞的資料放進 Context Component.Provider 的 value 中
        <ParentContext.Provider value={title}>
            <Children />
            {/* {props.children} */}
        </ParentContext.Provider>
    )
}

// 下層組件
// const Son = () => {
//     // 將 Context Component 放進 useContext 中取得 value 的資料
//     const title = useContext(ParentContext)
//     return (
//         // render 出 value 的資料
//         <h1>{title}</h1>
//     )
// }

export default UseContextStore;