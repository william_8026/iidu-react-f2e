import React, { createContext, useContext, useReducer } from 'react';

// useReducer(reducer, initialState)
// reducer 自訂 reducer action 內容
// initialState 自訂 state 初始內容

const productsInitState = { products: [] }
// 首先為了將 state 綁在一起，需要先建立一個 Global State Context
const ContextStore = createContext({
  products: [],
})

function productsReducer(state, action) {
  switch(action.type) {
    case 'ADD_PRODUCT':
      return {
        ...state,
        products: state.products.concat(
          { 
            id: state.products.length,
            name: "alex"  
          }
        ),
      }
    default:
      return state
  }
}

function Products() {
  const { products, productDispatch } = useContext(ContextStore);
  return (
    <div>
      {
        products.map(product => (<div key={product.id} >PRODUCT - {product.id} - {product.name}</div>))
      }
      <button onClick={() => productDispatch({type: 'ADD_PRODUCT'})}>ADD PRODUCT</button>
    </div>
  )
}

function UseContextStore() {
  const [productState, productDispatch] = useReducer(productsReducer, productsInitState); // (改變狀態， 初始狀態)
  console.log(productState)
  console.log(productDispatch)
  return (
    <ContextStore.Provider
      value={{
        products: productState.products,
        productDispatch,
      }}
    >
      <Products />
    </ContextStore.Provider>
  )
}


export default UseContextStore;