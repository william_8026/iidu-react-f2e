import React, { createContext, useContext, useReducer } from 'react';

// useReducer(reducer, initialState)
// reducer 自訂 reducer action 內容
// initialState 自訂 state 初始內容

// 建立 todos initial state
const todosInitialState = {
  todos: []
}
// 建立 reducer
function todosReducer(state, action) { // state 是 todosInitialState
  switch(action.type) {
    case 'ADD_TODO':
      // return Object.assign({}, state, {
      //   todos: state.todos.concat('eat')
      // })
      return {
        ...state,
        todos: state.todos.concat('william~~~')
      }

    default:
      return state
  }
}

// 使用 reducer hook
function UseContextStore() {
  const [state, dispatch] = useReducer(todosReducer, todosInitialState) // useReducer(執行內容, 初始內容)
  function handleClick() {
    dispatch({type: 'ADD_TODO'}) // 發送一個動作，到 todosReducer
  }
return (
    <>
      {
        state.todos.map((todo, index) => <div key={index}>{todo}</div>)
      }
      <button onClick={handleClick}>ADD TODO</button>
    </>
  )
}


export default UseContextStore;