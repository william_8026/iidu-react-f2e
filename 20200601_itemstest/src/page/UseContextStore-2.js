import React, { createContext, useContext, useReducer } from 'react';

// 建立一個 Context
const ContextStore = createContext({
  todos: [],
  william: ''
})

// 使用 ContextStore
function UseContextStore() {
  return (
    <ContextStore.Provider value={{todos: ['run123'], william: 'william123'}}>
      <Todos />
    </ContextStore.Provider>
  )
}

// Todos
function Todos() {
  // useContext(context: React.createContext)
  const value = useContext(ContextStore)
  // console.log(value)
  return (
    <>
      {
        value.todos.map(todo => <div key={todo}>{todo}</div>)
      }
    </>
  )
}

export default UseContextStore;